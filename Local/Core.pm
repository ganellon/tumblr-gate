package Local::Core;

use 5.010;

use strict;
use warnings;

use AnyEvent;
use AnyEvent::FCGI;
use Scalar::Util;

use Local::Log;

package Local::CGI {
require CGI;
@Local::CGI::ISA = 'CGI';
sub save_request { };
}

package Local::Core::context {

use Local::Log;
use Compress::Zlib ();

use constant {
    HTTP_OK             => 200,
    HTTP_NoContent      => 204,

    HTTP_Found          => 302,

    HTTP_BadRequest     => 400,
    HTTP_Unauthorized   => 401,
    HTTP_Forbidden      => 403,
    HTTP_NotFound       => 404,

    HTTP_InternalError  => 500,
    HTTP_NotImplemented => 501,
    HTTP_BadGateway     => 502,
};

# supported compress methods and theirs priorities
my %c_prio = (
    deflate => 1,
    gzip    => 2,
);
my @compressors = (
    # 0 => default/none
    undef,
    # 1 => deflate
    [ 'deflate', sub {
        my $z = Compress::Zlib::deflateInit(-Level => 3);
        $z->deflate($_[0]) . $z->flush()
    } ],
    # 2 => gzip
    [ 'gzip', sub {
        my $z = IO::Compress::Gzip->new(\my $r, Minimal => 1, -Level => 3);
        $z->write($_[0]); $z->flush; $z->close; $r
    } ],
);
sub find_encoder {
    $compressors[
        (
        sort { $a <=> $b }
        map { exists $c_prio{$_} ? $c_prio{$_} : () }
        map { lc }
        split /\s*,\s*/, ($_[0] || '')
        )[0] || 0
    ];
}

sub new {
    my ($class, $request) = @_;
    my $q = Local::CGI->new;

    my $self = bless {
        request => $request,
        response => [ HTTP_NotFound, 'Not found' ],

        q => $q,
        # request_uri and others parsed from ENV on request, so we need to
        # store them for future use, while we have localized %ENV
        info => {
            request_uri => $q->request_uri,
            script_name => $q->script_name,
        },
        encoder => find_encoder($q->http('accept-encoding')),
    }, $class;

    info  { sprintf ">>> request(0x%08x): uri = '%s'; path_info = '%s'; script_name = '%s'", +$self, $q->request_uri, $q->path_info, $q->script_name };
    trace { dumper ENV => \%ENV };

    $self
}

sub DESTROY {
    no warnings 'experimental';
    my ($self) = @_;
    my $response = $self->{response};
    my ($code, $answer) = (shift @$response, shift @$response);
    given ($code) {
        when (HTTP_OK) {
            if ($self->{encoder}) {
                my ($n, $e) = @{ $self->{encoder} };
                $answer = $e->(\$answer);
                push @$response, 'Content-Encoding' => $n;
            }
            push @$response,
            'Content-Type' => 'text/html; charset=utf-8',
            'Content-Length' => length $answer;
        }
        when (HTTP_Found) {
            push @$response, Location => $answer;
            $answer = '';
        }
        when (HTTP_NoContent) {
            push @$response,
            'Content-Type' => 'text/plain';
        }
    }

    info  { sprintf '<<< request(0x%08x): %d', +$self, $code };
    debug { my %h = @$response; map { "$_: $h{$_}" } sort keys %h };
    $self->{request}->respond($answer, Status => $code, @$response);
}

sub mkuri { join '/' => shift->{info}->{script_name}, @_ }

}

sub new {
    my ($pkg, %args) = @_;

    my $self = bless \%args, $pkg;

    $self->{route} ||= [
        sub {
            my ($self, $ctx) = @_;
            $ctx->{response} = [ $ctx->HTTP_OK, 'Good for you!' ];
        }
    ];

    my $o = $self; Scalar::Util::weaken($o);
    $self->{fcgi} = AnyEvent::FCGI->new(
        host => $self->{host} || '127.0.0.1',
        port => $self->{port} || 9000,
        on_request => sub {
            my $request = shift;

            local *STDIN; open STDIN, '<', \$request->read_stdin;
            local %ENV = %{$request->params};

            $o->process(Local::Core::context->new($request));
        }
    );

    $self
}

sub process {
    my ($self, $ctx) = @_;

    my (undef, @path) = split '/', $ctx->{q}->path_info;

    my ($i, $p) = (0, @path ? shift @path : '');
    my $pos = $self->{route};
    while ($i < @$pos) {
        my $e = $pos->[$i++];
        if (ref $e eq 'Regexp') {
            my ($name, $next) = @$pos[$i++, $i++];
            if ($p =~ $e) {
                $ctx->{$name} = $p;
                $pos = $next;
                ($i, $p) = (0, @path ? shift @path : '');
                next;
            }
        } elsif (ref $e eq 'CODE') {
            last if @path;
            eval { $self->$e($ctx, $p) };
            if ($@) {
                error { "!!! route endpoint died: $@" };
                $ctx->{response} = [ $ctx->HTTP_InternalError, 'Something went wrong' ]
            }
            last;
        } elsif ($e eq $p) {
            $ctx->{$p} = 1;
            $pos = $pos->[$i];
            ($i, $p) = (0, @path ? shift @path : '');
            next;
        }
    }
}

1;
