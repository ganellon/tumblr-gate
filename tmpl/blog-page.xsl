<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tumblr="urn:tumblr" exclude-result-prefixes="tumblr">

    <xsl:import href="common.xsl" />

    <xsl:template mode="header" match="tumblr">
        <xsl:apply-templates mode="header-photo" select="." />
        <title><xsl:value-of select="tumblelog/@title" /></title>
    </xsl:template>

    <xsl:template mode="content-header" match="tumblr">
        <header id="menu" class="menu">
            <a class="b switch"><span class="icon" /></a>
            <div class="drawer">
                <a class="b" href="{tumblr:url('home')}" title="Return to blog list">&#x2191;</a>
                <a id="autoload" class="b" title="Enable autoload on scroll">A</a>
                <a id="exclusive_zoom" class="b" title="Expanded image occupy whole row">E</a>
            </div>
        </header>
    </xsl:template>

    <xsl:template mode="body" match="tumblr">
        <h1><xsl:value-of select="tumblelog/@title" />&#160;<a class="ctrl xs" href="{tumblr:url('home')}">&#160;&#x2191;&#160;</a></h1>
        <div id="body">
            <xsl:apply-templates select="posts/post"><xsl:sort select="@unix-timestamp" /></xsl:apply-templates>
        </div>
        <div id="navigation">
            <xsl:text>[page </xsl:text>
            <xsl:value-of select="tumblr:pager_cur()" />
            <xsl:text>/</xsl:text>
            <xsl:value-of select="tumblr:pager_last()" />
            <xsl:text>]</xsl:text>
            <xsl:if test="tumblr:pager_cur() &gt; 1">
                <a class="ctrl" rel="prev" href="{tumblr:pager_url(-1)}">&#160;&#x2190;&#160;</a>
            </xsl:if>
            <a class="ctrl" href="{tumblr:url('home')}">&#160;&#x2191;&#160;</a>
            <xsl:if test="tumblr:pager_cur() &lt; tumblr:pager_last()">
                <a class="ctrl" rel="next" href="{tumblr:pager_url( 1)}">&#160;&#x2192;&#160;</a>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="post">
        <xsl:choose>
            <xsl:when test="photoset">
                <xsl:apply-templates select="photoset/photo" mode="photorow" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="." mode="photorow" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*" mode="photorow">
        <xsl:variable name="pnum"><xsl:number count="post" /></xsl:variable>
        <div class="item">
            <div class="item-container">
                <div class="photo">
                    <a href="{photo-url[@max-width=1280]/text()}">
                        <img src="{photo-url[@max-width=250]/text()}" alt="" />
                    </a>
                </div>
                <div class="toolbar">
                    <table class="layout">
                        <tr>
                            <td class="t">
                                <a class="ctrl star fav" href="{tumblr:url('blog')}/_digest/{ancestor-or-self::post/@id}/{concat(@offset, substring('o1', 1 div not(@offset)))}">&#x2605;</a>
                                <a class="ctrl" href="{ancestor-or-self::post/@url}">&#x2302;</a>
                                <xsl:if test="ancestor-or-self::post/photo-link-url">
                                    <a class="ctrl" href="{ancestor-or-self::post/photo-link-url/text()}">&#x2197;</a>
                                </xsl:if>
                            </td>
                            <td class="ctrl s c vc">#<xsl:value-of select="tumblr:post() + 1 + count(//posts/post) - $pnum" /></td>
                            <td class="t">
                                <a class="ctrl folder" toggle="&#x2303;">&#x2304;</a>
                            </td>
                        </tr>
                    </table>
                    <table class="layout fold">
                        <tr class="s"><td class="ctrl xs">post <a class="ctrl" href="{ancestor-or-self::post/@url}"><xsl:value-of select="ancestor-or-self::post/@url" /></a></td></tr>
                        <xsl:if test="ancestor-or-self::post/photo-link-url">
                            <tr class="s"><td class="ctrl xs">link <a class="ctrl" href="{ancestor-or-self::post/photo-link-url/text()}"><xsl:value-of select="ancestor-or-self::post/photo-link-url/text()" /></a></td></tr>
                        </xsl:if>
                        <tr class="s"><td class="ctrl xs"><xsl:value-of select="tumblr:format_date(ancestor-or-self::post/@unix-timestamp)" /></td></tr>
                        <xsl:if test="ancestor-or-self::post/photo-caption">
                            <tr><td><xsl:value-of select="tumblr:filter(ancestor-or-self::post/photo-caption/text())" disable-output-escaping="yes" /></td></tr>
                        </xsl:if>
                    </table>
                </div>
            </div>
        </div>
    </xsl:template>

</xsl:stylesheet>
