<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tumblr="urn:tumblr" exclude-result-prefixes="tumblr">

    <xsl:import href="common.xsl" />

    <xsl:template mode="header" match="tumblr">
        <script type="text/javascript" src="/static/x/sorttable.js" />
        <script type="text/javascript" src="/static/list.js" />
        <script type="text/javascript">$(function () { $.sessions.add('<xsl:value-of select="@id" />') })</script>
        <title>Tumblr Gate :: <xsl:value-of select="@id" /></title>
    </xsl:template>

    <xsl:template mode="body" match="tumblr">
        <h1>Last visited tumblrs</h1>
        <table class="list sortable">
            <thead>
                <tr>
                    <th>Blog</th>
                    <th>Title</th>
                    <th>New Posts</th>
                    <th>Total Posts</th>
                    <th class="sorttable_nosort">&#160;</th>
                </tr>
            </thead>
            <tfoot>
                <tr><td colspan="5"><a class="ctrl xs" id="add">add blog</a></td></tr>
            </tfoot>
            <tbody>
                <xsl:apply-templates select="blog"><xsl:sort select="@name" /></xsl:apply-templates>
            </tbody>
        </table>
        <h1>&#160;</h1>
        <h1>Own digests</h1>
        <table class="list">
            <thead>
                <tr>
                    <th>Week</th>
                    <th>Image count</th>
                    <th>Secure link</th>
                    <th>&#160;</th>
                </tr>
            </thead>
            <tbody>
                <xsl:apply-templates select="digest"><xsl:sort select="@name" /></xsl:apply-templates>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="blog">
        <tr>
            <xsl:if test="not(number(@enabled))">
                <xsl:attribute name="class">disabled</xsl:attribute>
            </xsl:if>
            <td><a href="{@url}" title="{@title}"><xsl:value-of select="@name" /></a></td>
            <td><xsl:value-of select="@title" /></td>
            <td class="r"><xsl:value-of select="@new" /></td>
            <td class="r"><xsl:value-of select="@posts" /></td>
            <td><a class="ctrl s" href="{@url}/../delete">&#x274c;</a></td>
        </tr>
    </xsl:template>

    <xsl:template match="digest">
        <tr>
            <td><a href="{@url}" title="{@issue}/{@year}"><xsl:value-of select="@issue" />/<xsl:value-of select="@year" /></a></td>
            <td class="r"><xsl:value-of select="@count" /></td>
            <td class="c">
                <a class="ctrl s" href="{@share}">
                    <xsl:choose>
                        <xsl:when test="number(@public)">&#x2197;</xsl:when>
                        <xsl:otherwise>publish</xsl:otherwise>
                    </xsl:choose>
                </a>
            </td>
            <td><a class="ctrl s" href="{@url}/delete">&#x274c;</a></td>
        </tr>
    </xsl:template>

</xsl:stylesheet>
