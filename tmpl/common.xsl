<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tumblr="urn:tumblr" exclude-result-prefixes="tumblr">
    <xsl:output
        method="xml"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        indent="yes"
        />

    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
                <link rel="stylesheet" type="text/css" href="/static/gate.css" />
                <script type="text/javascript" src="/static/x/jquery.min.js" />
                <script type="text/javascript" src="/static/sessions.js" />
                <xsl:apply-templates mode="header" />
                <meta property="og:site_name" content="Tumblr Gate" />
                <meta property="og:type" content="website" />
            </head>
            <body>
                <xsl:apply-templates mode="content-header" />

                <xsl:apply-templates mode="body" />

                <xsl:apply-templates mode="content-footer" />
            </body>
        </html>
    </xsl:template>

    <xsl:template mode="header" match="*">
        <title>Something going wrong here</title>
    </xsl:template>

    <xsl:template mode="content-header" match="*" />

    <xsl:template mode="body" match="*" />

    <xsl:template mode="content-footer" match="*">
        <footer class="signature"><a href="https://bitbucket.org/ganellon/tumblr-gate/">&#169;&#160;ganellon</a></footer>
    </xsl:template>

    <xsl:template mode="header-photo" match="*">
        <script type="text/javascript" src="/static/x/masonry.pkgd.min.js" />
        <script type="text/javascript" src="/static/fontsloaded.js" />
        <script type="text/javascript" src="/static/gate.js" />
    </xsl:template>

</xsl:stylesheet>
