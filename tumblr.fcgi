#!/usr/bin/perl

use strict;
use warnings;

use AnyEvent;

use FindBin;
use lib $FindBin::Bin;

use Local::Log {
    level => 'debug',
    file  => "$FindBin::Bin/log/tumblr.log",
};

use Local::Tumblr;

my $fcgi = Local::Tumblr->new(
    host => '127.0.0.1',
    port => 9000,
    db_path => "$FindBin::Bin/db/tumblr.db",
    templates => {
        parking => "$FindBin::Bin/tmpl/parking.xsl",
        list    => "$FindBin::Bin/tmpl/blog-list.xsl",
        page    => "$FindBin::Bin/tmpl/blog-page.xsl",
        digest  => "$FindBin::Bin/tmpl/blog-digest.xsl",
    },
    CACHE_TIMEOUT => 86400,
);

my $main = AnyEvent->condvar;

warn "server starting...\n";
$SIG{HUP} = sub { Local::Log::reopen(); };
$SIG{TERM} = $SIG{INT} = sub { warn "server exiting...\n"; $main->send };
$main->recv;
