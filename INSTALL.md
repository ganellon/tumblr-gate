# Installation

## Clone repo
Choose folder where project will live. We will reference to this folder as
`PATH2REPO`. Clone repo into it:
`$ cd PATH2REPO && git clone https://bitbucket.org/ganellon/tumblr-gate.git .`

## Install prerequisites (any posix)
1. Web server with fastcgi support
2. Direct Perl dependencies:
  - AnyEvent
  - AnyEvent::HTTP
  - AnyEvent::FCGI
  - XML::LibXML
  - XML::LibXSLT
  - DBI
  - DBD::SQLite

## Install dependencies (Debian)
1. apt-get install libanyevent-perl libanyevent-http-perl libxml-libxml-perl libxml-libxslt-perl libdbi-perl libdbd-sqlite3-perl libapache2-mod-fastcgi
2. Since debian lacks all packages, install following deps manually:
   - `cpan install AnyEvent::FCGI`

## Configuration

### Fix permissions
Folders `PATH2REPO/log` and `PATH2REPO/db` must be writable by fastcgi process.
Folder `PATH2REPO/static` must be readable by web server.

### Sample web server configuration
`HOSTNAME` here is a hostname on which gate will reside. Note: some browsers do
not apply css if server serve it with wrong content-type.

#### lighttpd
```config
server.modules += ( "mod_fastcgi" )
$HTTP["host"] == "HOSTNAME" {
    $HTTP["url"] =~ "^/(static|favicon.ico)" {
        alias.url = ( "/" => "PATH2REPO/" )
    }
    else $HTTP["url"] =~ "^/" {
        fastcgi.server = (
            "/" =>
            ((
              "host" => "127.0.0.1",
              "port" => 9000,
              "check-local" => "disable",
              "fix-root-scriptname" => "enable",
              "docroot" => "PATH2REPO/static/",
              "allow-x-send-file" => "enable",
            ))
        )
    }
}
```

### apache
```config
<VirtualHost *:80>
    ServerName HOSTNAME
    ServerAlias www.HOSTNAME

    Alias /tumblr         "PATH2REPO/tumblr.fcgi"
    FastCgiExternalServer "PATH2REPO/tumblr.fcgi" -host 127.0.0.1:9000

    DocumentRoot PATH2REPO/static/

    <Directory "PATH2REPO/static">
        Options -Indexes MultiViews FollowSymlinks
        AllowOverride None
        Order allow,deny
        Allow from all
    </Directory>

</VirtualHost> 
```

### Initialize database
For database initialization use schema from repo. For our simple configuration
just run `sqlite3 PATH2REPO/db/tumblr.db < PATH2REPO/Local/db.sql`.

## Final step
Start FastCGI server by running `PATH2REPO/tumblr.fcgi` or write autostart
service for your system and you are ready to serve queries.
