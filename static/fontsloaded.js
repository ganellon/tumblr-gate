(function ($) {

var fontCache = {};

FontWatcher.prototype = {

on : function (type, cb) {
    this.cb[type].push(cb);
    // fire event if it backed by property that have value
    if (typeof this[type] !== 'undefined')
        this.emit(type, this[type]);
},

off : function (type, cb) {
    this.cb[type].splice(this.cb[type].indexOf(cb), 1);
},

_emit : function (type, timer, args) {
    delete this[timer];
    var list = this.cb[type].slice(0);
    for (var i in list)
        list[i].apply(this, args);
},

emit : function (type) {
    var timer = type + 'Timer';
    if (!this[timer] && this.cb[type].length) {
        var _this = this,
            args  = Array.prototype.slice.call(arguments, 1);
        this[timer] = setTimeout(function () { _this._emit(type, timer, args) });
    }
},

fontFamily : 'sans-serif',

};

function FontWatcher (font) {
    this.font = font;
    this.cb = { ready : [] };

    // replay true on font used as initial
    if (/^[a-z\-]+$/.test(font)) {
        this.emit('ready', this.ready = true);
        return;
    }

    // prepare blocks
    this.main = document.createElement('div');
    this.main.style.cssText = 'position: absolute; left: 0; top: -1000px; font-size: 300px; font-family: '+this.fontFamily;
    this.main.innerHTML = 'aAiIjJmMzZ01379';

    // make text visible, so it can be measured
    document.body.appendChild(this.main);
    this.height = this.main.offsetHeight;

    var _this = this;
    this.timer = setInterval(function () {
        if (_this.main.offsetHeight == _this.height) return;

        clearInterval(_this.timer);
        _this.ready = true;
        _this.emit('ready', true);

        _this.main.parentNode.removeChild(_this.main);
        _this.main = null;
        delete _this.main;
    }, 50);
    setTimeout(function () { clearInterval(_this.timer) }, 30000);

    this.main.style.fontFamily = font;
}

function getFontWatcher (font) {
    if (!fontCache[font])
        fontCache[font] = new FontWatcher(font);
    return fontCache[font];
}

function parseFonts (e, l) {
    window
        .getComputedStyle(e, null) // css object
        .fontFamily                // string property
        .split(/, */)              // array of strings
        .forEach(function (f) { l[f] = true });
}

$.fn.fontsLoaded = function (cb, checkChilds) {
    var defer = $.Deferred();
    if (cb) defer.always(cb);

    if (typeof checkChilds === 'undefined') checkChilds = true;

    var fonts = {};
    this.each(function () {
        parseFonts(this, fonts);
        if (checkChilds)
            $(this).find('*').each(function () { parseFonts(this, fonts) });
    });

    var num = 0;
    var timeout = setTimeout(function () { defer.reject() }, 5000);
    var ready_cb = function () {
        this.off('ready', ready_cb);
        defer.notify(this.font);
        if (--num == 0) {
            clearTimeout(timeout);
            defer.resolve();
        }
    };

    for (var font in fonts) {
        ++num;
        getFontWatcher(font).on('ready', ready_cb);
    }

    return defer.promise();
};

})(jQuery);
